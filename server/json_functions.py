import json


def path(arg: list) -> str:
    with open('data.json', 'r') as data:
        new_arg = json.load(data)
    result = ''
    try:
        for i in arg:
            if isinstance(i, dict):
                key = list(i.keys())[0]
                for j in new_arg:
                    try:
                        if j[key] == i[key]:
                            result += f'[{new_arg.index(j)}]'
                            new_arg = j
                            break
                    except KeyError:
                        pass
                else:
                    return ''
            else:
                if isinstance(i, int):
                    result += f'[{i}]'
                else:
                    result += f'["{i}"]'
                new_arg = new_arg[i]
    except (KeyError, TypeError):
        return ''
    return result


def read_json(arg: list) -> list:
    with open('data.json', 'r') as data:
        new_arg = json.load(data)
    par = path(arg)
    if par:
        result = eval(f"new_arg{path(arg)}")
        return [result]
    return ['']


def rewrite_json(arg: list, value) -> bool:
    with open('data.json', 'r') as data:
        new_arg = json.load(data)
    par = path(arg)
    if par:
        try:
            exec(f"new_arg{path(arg)} = value")
        except IndexError:
            return False
        with open('data.json', 'w') as data:
            json.dump(new_arg, data)
        return True
    return False


def input_json(arg: list, value) -> bool:
    with open('data.json', 'r') as data:
        new_arg = json.load(data)
    par = [path(arg)]
    if par:
        try:
            exec(f"new_arg{path(arg)}.append(value)")
        except IndexError:
            return False
        with open('data.json', 'w') as data:
            json.dump(new_arg, data)
        return True
    return False
