from flask import Flask, request, jsonify, make_response
import json_functions
import random
import json

app = Flask(__name__)
token_symbols = r"qwertyuiopasdfghjklzxcvbnmQWER" \
                r"TYUIOPASDFGHJKLZXCVBNM1234567890|{}-_=+./[]"


def add_queue(user_id, token):
    with open('queue.json', 'r') as data:
        queue = json.load(data)
        ids = [x["id"] for x in queue["in_queue"]]
        if user_id in ids:
            queue["in_queue"][ids.index(user_id)] =\
                {"id": user_id, "token": token}
        else:
            queue["in_queue"].append({"id": user_id, "token": token})
    with open('queue.json', 'w') as data:
        json.dump(queue, data)
    return True


def get_id(token):
    with open('queue.json', 'r') as data:
        queue = json.load(data)
        for i in queue["in_queue"]:
            if i['token'] == token:
                return i['id']


def make_token():
    result = ''
    for i in range(32):
        result += token_symbols[random.randint(0, len(token_symbols) - 1)]
    return result


@app.route("/", methods=['GET', 'POST'])
def stats():
    cook = request.cookies.get('token')
    if not cook:
        return entry()
    return make_response("Already entry", 200)


@app.route("/login/<data_json>", methods=['GET', 'POST'])
def entry():
    data_json = request.json
    if not data_json:
        return make_response('Need autorize', 401)
    try:
        login = data_json['login']
        password = data_json['password']
    except KeyError:
        return make_response('Wrong data', 401)
    if login == 'admin':
        return make_response('Sorry, maybe in the future ((', 501)
    user_id = json_functions.read_json(['users', {"login": login}, "id"])[0]
    if type(user_id) != int:
        return make_response('User not found', 401)
    if password != json_functions.read_json(['users',
                                             {"id": user_id}, 'password'])[0]:
        return make_response('Wrong password', 401)
    cook = make_response('Set cookies', 201)
    tkn = make_token()
    add_queue(user_id, tkn)
    cook.set_cookie('token', tkn, max_age=None)
    return cook


@app.route("/register", methods=['GET', 'POST'])
def register():
    data_json = request.json
    if not data_json:
        return make_response('Need autorize', 401)
    try:
        login = data_json['login']
        password = data_json['password']
    except KeyError:
        return make_response('Wrong data', 401)
    if type(json_functions.read_json(['users',
                                      {"login": login}, "id"])[0]) == int:
        return make_response('Login busy', 401)
    start_cap = json_functions.read_json(['settings', 'start_cap'])[0]
    with open('data.json', 'r') as data:
        id = max([x["id"] for x in json.load(data)["users"]]) + 1
    json_functions.input_json(
        ['users'], {'id': id, 'login': login, 'password': password,
                    'stats': {"balance": start_cap, "games": []}})
    return make_response('Registration True', 201)


@app.route("/game", methods=['GET', 'POST'])
def game():
    token = request.cookies.get('token')
    if not token:
        return make_response('Need autorize', 401)
    min_dep = json_functions.read_json(['settings', 'min_dep'])[0]
    user_id = get_id(token)
    values = request.json['values']
    if len(values) == 2 and type(values[0]) == int and values[1] in [1, 2, 3]:
        if values[0] < min_dep:
            return make_response('Wrong deposite.' + str(min_dep), 401)
        balance = json_functions.read_json(
            ['users', {"id": user_id}, 'stats', 'balance'])[0]
        game_balance = json_functions.read_json(
            ["users", 0, "stats", "balance"])[0]
        if balance < values[0]:
            return make_response('Not enough money', 401)
        if random.randint(1, 3) == values[1]:
            json_functions.rewrite_json(
                ['users', {"id": user_id}, 'stats', 'balance'],
                balance + values[0] * 2)
            json_functions.rewrite_json(
                ['users', 0, 'stats', 'balance'], game_balance - values[0])
            return make_response('Game win', 201)
        else:
            json_functions.rewrite_json(
                ['users', {"id": user_id}, 'stats', 'balance'],
                balance - values[0])
            json_functions.rewrite_json(
                ['users', 0, 'stats', 'balance'], game_balance + values[0])
            return make_response('Game lose', 201)
    else:
        return make_response('Wrong data', 401)


@app.route("/leaders", methods=['GET', 'POST'])
def leaders():
    result = []
    for i in json.load(open('data.json', 'r'))['users'][1:]:
        result.append([i['login'], i['stats']['balance']])
    result = sorted(result, key=lambda x: x[1], reverse=True)
    if len(result) > 10:
        result = result[:10]
    for i in range(len(result)):
        result[i] = {"login": result[i][0], "balance": result[i][1]}
    return jsonify({"result": result}), 200


@app.route("/balance", methods=['GET', 'POST'])
def get_balance():
    token = request.cookies.get('token')
    if not token:
        return make_response('Need autorize', 401)
    user_id = get_id(token)
    balance = json_functions.read_json(
        ["users", {"id": user_id}, 'stats', 'balance'])[0]
    return make_response(str(balance), 200)


if __name__ == '__main__':
    app.run(host='', port=8000, debug=True)
