import requests

global req


HOST = f'http://localhost:8000/'


def start():
    true = True
    while true:
        print("Авторизуйтесь")
        login = input('login: ')
        password = input('password: ')
        if entry(login, password):
            true = False
            print('Успешная авторизация')
    return True


def entry(login, password):
    res = req.post(HOST, json={"login": login, "password": password})
    if res.status_code == 501:
        print('Аккаунт администратора пока не доступен')
        return False
    if res.text == 'User not found':
        print('Пользователь не найден')
        ans = input('Хотите зарегистировать аккаунт? (Y - да) ').lower()
        if ans == 'y':
            req.post(HOST + 'register', json={"login": login,
                                              "password": password})
            print('Регистарция завершена')
        return False
    if res.text == 'Wrong password':
        print('Неверный пароль')
        return False
    return True


def game(command):
    dep = command[2:]
    try:
        dep = int(dep)
    except (TypeError, ValueError):
        print('Неверное значение')
        return False
    print('Для отмены введите неверное значение')
    choice = input('Введите число от 1 до 3: ')
    try:
        choice = int(choice)
    except (TypeError, ValueError):
        print('Отменено')
        return False
    if choice not in [1, 2, 3]:
        print('Отменено')
        return False
    res = req.post(HOST + 'game', json={"values": [dep, choice]})
    if res.text == 'Not enough money':
        print('Недостаточно денег')
        return False
    elif res.text.split('.')[0] == 'Wrong deposite':
        print('Cтавка не может быть меньше', res.text.split('.')[1], 'монет')
        return False
    elif res.text == 'Game win':
        print('Поздравляю, вы выиграли', dep * 2, 'монет')
        return True
    print('Жаль, но вы проиграли', dep, 'монет')
    return True


def commands():
    print('''Доступные команды:
1           - Проверить свой баланс
2           - Показать топ игроков 10 по балансу
3 *сумма*   - Начать игру в напёрстки
4           - показать список доступный команд
5           - для выхода из аккаунта''')
    return True


def leaders():
    res = req.post(HOST + 'leaders').json()
    k = 1
    for i in res['result']:
        print(str(k) + '.', i["login"], "-", i["balance"])
        k += 1


if __name__ == '__main__':
    req = requests.Session()
    start()
    commands()
    true = True
    while true:
        command = input()
        if command == '1':
            print('Баланс:', req.post(HOST + 'balance').text, 'монет')
        elif command == '2':
            leaders()
        elif command and command[0] == '3':
            game(command)
        elif command == '4':
            commands()
        elif command == '5':
            req = requests.Session()
            start()
            commands()
        else:
            print('Я не знаю такую команду (4 - для помощи)')
